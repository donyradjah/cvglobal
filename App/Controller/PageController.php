<?php
/**
 * Created by PhpStorm.
 * User: Imam
 * Date: 2/2/2017
 * Time: 8:36 AM
 */

namespace Controller;


use Config\View;

class PageController
{

    public function admin()
    {
        View::render("layouts.admin");
    }
    public function programmer()
    {
        View::render("layouts.programmer");
    }

}