<?php
/**
 * Created by PhpStorm.
 * User: Imam
 * Date: 1/31/2017
 * Time: 10:00 AM
 */

namespace Controller;


use Config\Config;
use Config\View;
use Model\Paket;
use Model\Pendaftar;
use Model\Tool;
use Particle\Validator\Validator;

class PendaftarController
{
    private $pendaftar;
    private $tool;
    private $paket;

    /**
     * PendaftarController constructor.
     * @param $pendaftar
     */
    public function __construct()
    {
        $this->pendaftar = new Pendaftar();
        $this->tool = new Tool();
        $this->paket = new Paket();
    }

    public function delete($id,$type, $stat)
    {
        $pendaftar = $this->pendaftar->detail($id)['data'];
        unlink($pendaftar['img']);
        unlink($pendaftar['penjelasan']);
        $delete = $this->pendaftar->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/pendaftar/" . $type . "/" . $stat . "/list'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/pendaftar/" . $type . "/" . $stat . "/list'</script>";
        }
    }

    public function index()
    {
        $data = $this->pendaftar->all();
        $data['type'] = 1;
        View::render("pendaftar.list", $data);
    }

    public function indexbytype($type, $stat)
    {
        $data = $this->pendaftar->allbytype($type, $stat);
        $data['type'] = $type;
        $data['stat'] = $stat;
        View::render("pendaftar.list", $data);
    }

    public function edit($id, $type, $stat, $data = array())
    {
        $asalsekolah = $this->pendaftar->detail($id);
        if (count($asalsekolah['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($asalsekolah['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $asalsekolah['message'],
                    "data"    => $asalsekolah['data'],
                ];
            } else {
                $arr = [
                    "success" => ($asalsekolah['success']),
                    "message" => $asalsekolah['message'],
                    "data"    => $asalsekolah['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        $arr['tool'] = $this->tool->all();
        $arr['paket'] = $this->paket->all();
        $arr['type'] = $type;
        $arr['stat'] = $stat;
        \Config\View::render('pendaftar.create', $arr);
    }

    public function create($type, $stat, $data = array())
    {
        $data['tool'] = $this->tool->all();
        $data['paket'] = $this->paket->all();
        $data['type'] = $type;
        $data['stat'] = $stat;

        \Config\View::render('pendaftar.create', $data);
    }

    public function insert($type, $stat, $data = array(), $img = array(), $jls = array())
    {

        $v = new Validator;
        $v->overwriteDefaultMessages(Config::error_message_data_master());
        $v->required('nama')->lengthBetween(5, 50);
        $v->optional('nim')->numeric();
        $v->required('alamat');
        $v->required('telepon')->numeric();
        $v->optional('asal_kampus')->string();
        $v->required('email')->email();
        $v->required('judul')->string();
        $v->required('paket');
        $v->required('harga');
        $v->required('agreements');
        $v->required('status');
        $v->optional('img')->string();
        $v->required('nominal')->numeric();
        $v->required('type');
        $v->optional('penjelasan')->string();
        $v->required('tool');
        $v->optional('catatan')->string();
        $result = $v->validate($data);
        if ($result->isNotValid()) {
            $arr['success'] = false;
            $arr['message'] = "<ul>";
            $arr['data'] = $data;
            foreach ($result->getMessages() as $e) {
                foreach ($e as $key => $value) {
                    $arr['message'] .= "<li>" . $value . "</li>";
                }
            }
            $arr['message'] .= "</ul>";
            $this->create($type, $arr);
        } else {
            if (isset($jls['name']) && $jls['name'] != "") {
                $ext = pathinfo($jls['name'], PATHINFO_EXTENSION);
                $arr1 = str_replace(' ', '', strtolower($data['nama']));
                $ktpurl = 'dokumen_pendaftar/' . $arr1 . date('dmYhi') . "." . $ext;
                move_uploaded_file($jls['tmp_name'], $ktpurl);
                $data['penjelasan'] = $ktpurl;
            }
            if (isset($img['name']) && $img['name'] != "") {
                $ext = pathinfo($img['name'], PATHINFO_EXTENSION);
                $arr1 = str_replace(' ', '', strtolower($data['nama']));
                $ktpurl = 'bukti_pembayaran/' . $arr1 . date('dmYhi') . "." . $ext;
                move_uploaded_file($img['tmp_name'], $ktpurl);
                $data['img'] = $ktpurl;
            }
            $insert = $this->pendaftar->insert($data);
            if ($insert['success']) {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/pendaftar/" . $type . "/" . $stat . "/list'</script>";
            } else {
                $this->create($type, $stat, $insert);
            }
        }

    }

    public function update($id, $type, $stat, $data = array(), $img = array(), $jls = array())
    {
        $pendaftar = $this->pendaftar->detail($id);
        $v = new Validator;
        $v->overwriteDefaultMessages(Config::error_message_data_master());
        $v->required('nama')->lengthBetween(5, 50);
        $v->optional('nim')->numeric();
        $v->required('alamat');
        $v->required('telepon')->numeric();
        $v->optional('asal_kampus')->string();
        $v->required('email')->email();
        $v->required('judul')->string();
        $v->required('paket');
        $v->required('harga');
        $v->required('agreements');
        $v->required('status');
        $v->optional('img')->string();
        $v->required('nominal')->numeric();
        $v->required('type');
        $v->optional('penjelasan')->string();
        $v->required('tool');
        $v->optional('catatan')->string();
        $result = $v->validate($data);
        if ($result->isNotValid()) {
            $arr['success'] = false;
            $arr['message'] = "<ul>";
            $arr['data'] = $data;
            foreach ($result->getMessages() as $e) {
                foreach ($e as $key => $value) {
                    $arr['message'] .= "<li>" . $value . "</li>";
                }
            }
            $arr['message'] .= "</ul>";
            $this->create($type, $arr);
        } else {
            if (isset($jls['name']) && $jls['name'] != "") {
                $ext = pathinfo($jls['name'], PATHINFO_EXTENSION);
                $arr1 = str_replace(' ', '', strtolower($data['nama']));
                $ktpurl = 'dokumen_pendaftar/' . $arr1 . date('dmYhi') . "." . $ext;
                move_uploaded_file($jls['tmp_name'], $ktpurl);
                $data['penjelasan'] = $ktpurl;
            } else {
                $data['penjelasan'] = $pendaftar['data']['penjelasan'];
            }
            if (isset($img['name']) && $img['name'] != "") {
                $ext = pathinfo($img['name'], PATHINFO_EXTENSION);
                $arr1 = str_replace(' ', '', strtolower($data['nama']));
                $ktpurl = 'bukti_pembayaran/' . $arr1 . date('dmYhi') . "." . $ext;
                move_uploaded_file($img['tmp_name'], $ktpurl);
                $data['img'] = $ktpurl;
            } else {
                $data['img'] = $pendaftar['data']['img'];
            }
            $insert = $this->pendaftar->insert($data);
            if ($insert['success']) {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/pendaftar/" . $type . "/" . $stat . "/list'</script>";
            } else {
                $this->edit($id, $type, $stat, $insert);
            }
        }

    }
}