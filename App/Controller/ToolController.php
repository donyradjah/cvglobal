<?php

namespace Controller;

use Config\Config;
use Model\Tool;
use Particle\Validator\Validator;

/**
 * Created by PhpStorm.
 * User: Dony
 * Date: 1/15/2017
 * Time: 9:22 PM
 */
class ToolController
{
    private $tool;

    /**
     * StatusWaliController constructor.
     * @param $statuswali
     */
    public function __construct()
    {
        $this->tool = new Tool();
    }

    public function all()
    {
        $data = $this->tool->all();
        if ($data['success']) {
            return [
                'status' => 200,
                'data'   => $data['data'],
            ];
        } else {
            return [
                'status' => 500,
                'data'   => $data['message'],
            ];
        }
    }

    public function store($data = array())
    {
        $v = new Validator;
        $v->overwriteDefaultMessages(Config::error_message_data_master());
        $v->required('tool');
        $result = $v->validate($data);
        if ($result->isNotValid()) {
            return [
                'status'  => 500,
                'message' => $result->getMessages(),
            ];
        } else {
            $insert = $this->tool->insert($data);
            if ($insert['success']) {
                return [
                    'status'  => 200,
                    'message' => "Berhasil Menyimpan Data",
                ];
            } else {
                return [
                    'status'  => 500,
                    'message' => $insert['message'],
                ];
            }
        }
    }

    public function update($id, $data = array())
    {


    }

    public function delete($id)
    {

    }
}