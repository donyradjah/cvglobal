<?php
/**
 * Created by PhpStorm.
 * User: Imam
 * Date: 2/2/2017
 * Time: 9:22 AM
 */

namespace Model;


use Config\Config;
use PDO;
use PDOException;

class Pembayaran
{
    private $db;

    function __construct()
    {
        $this->db = Config::getConnection();
    }

    public function all()
    {
        try {
            $query = "SELECT pendaftar.*,paket.nama_paket FROM pendaftar,paket WHERE pendaftar.paket = paket.id  and ISNULL(pendaftar.deleted_at)  ORDER by pendaftar.created_at DESC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function allbytype($type, $stat)
    {
        try {
            $query = "SELECT pendaftar.*,paket.nama_paket FROM pendaftar,paket WHERE pendaftar.paket = paket.id and type=:type and status=:stat  and ISNULL(pendaftar.deleted_at)  ORDER by pendaftar.created_at DESC";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':type', $type);
            $stmt->bindParam(':stat', $stat);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function detail($id)
    {
        try {
            $query = "SELECT pendaftar.*,paket.nama_paket FROM pendaftar,paket WHERE pendaftar.paket = paket.id and pendaftar.id=:id and ISNULL(pendaftar.deleted_at)  ORDER by pendaftar.created_at ASC";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function insert($data)
    {
        try {
            $query = "SELECT no_urut FROM pendaftar WHERE ISNULL(deleted_at) and paket = $data[paket] ORDER by no_urut DESC LIMIT 1";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch();
            $no_urut = $array['no_urut'] + 1;
            $sql = "INSERT INTO pendaftar VALUES 
          (NULL,:nama,:nim,:alamat,:telepon,:asal_kampus,:email,:judul,:paket,:harga,:agreements,:status,:img,:nominal,:type,:penjelasan,:tool,:created_at,NULL ,NULL ,:catatan,:tanggal,:no_urut)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':nim', $data['nim']);
            $stmt->bindparam(':alamat', $data['alamat']);
            $stmt->bindparam(':telepon', $data['telepon']);
            $stmt->bindparam(':asal_kampus', $data['asal_kampus']);
            $stmt->bindparam(':email', $data['email']);
            $stmt->bindparam(':judul', $data['judul']);
            $stmt->bindparam(':paket', $data['paket']);
            $stmt->bindparam(':harga', $data['harga']);
            $stmt->bindparam(':agreements', $data['agreements']);
            $stmt->bindparam(':status', $data['status']);
            $stmt->bindparam(':img', $data['img']);
            $stmt->bindparam(':nominal', $data['nominal']);
            $stmt->bindparam(':type', $data['type']);
            $stmt->bindparam(':penjelasan', $data['penjelasan']);
            $stmt->bindparam(':tool', $data['tool']);
            $stmt->bindparam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':catatan', $data['catatan']);
            $stmt->bindparam(':tanggal', date('d-m-Y'));
            $stmt->bindparam(':no_urut', $no_urut);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $sql = "DELETE FROM pendaftar  where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);

            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function update($id, $data)
    {
        try {
            $sql = "UPDATE pendaftar set  nama=:nama,
                                          nim=:nim,
                                          alamat=:alamat,
                                          telepon=:telepon,
                                          asal_kampus=:asal_kampus,
                                          email=:email,
                                          judul=:judul,
                                          paket=:paket,
                                          harga=:harga,
                                          agreements=:agreements,
                                          status=:status,
                                          img=:img,
                                          nominal=:nominal,
                                          type=:type,
                                          penjelasan=:penjelasan,
                                          tool=:tool,
                                          updated_at=:updated_at,
                                          catatan=:catatan WHERE id= :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $data['id']);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':nim', $data['nim']);
            $stmt->bindparam(':alamat', $data['alamat']);
            $stmt->bindparam(':telepon', $data['telepon']);
            $stmt->bindparam(':asal_kampus', $data['asal_kampus']);
            $stmt->bindparam(':email', $data['email']);
            $stmt->bindparam(':judul', $data['judul']);
            $stmt->bindparam(':paket', $data['paket']);
            $stmt->bindparam(':harga', $data['harga']);
            $stmt->bindparam(':agreements', $data['agreements']);
            $stmt->bindparam(':status', $data['status']);
            $stmt->bindparam(':img', $data['img']);
            $stmt->bindparam(':nominal', $data['nominal']);
            $stmt->bindparam(':type', $data['type']);
            $stmt->bindparam(':penjelasan', $data['penjelasan']);
            $stmt->bindparam(':tool', $data['tool']);
            $stmt->bindparam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':catatan', $data['catatan']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }
}