<?php
/**
 * Created by PhpStorm.
 * User: Imam
 * Date: 1/3/2017
 * Time: 12:07 PM
 */

namespace Config;


class View
{
    public static function render($file, $variables = array())
    {
        extract($variables);
        $file = explode(".", $file);
        include "View/layouts/header.php";
        include "View/layouts/sidebar.php";
        include "View/" . implode("/", $file) . ".php";
    }

}