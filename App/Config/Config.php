<?php
/**
 * Created by PhpStorm.
 * User: global
 * Date: 1/6/17
 * Time: 10:52 AM
 */

namespace Config;


use Particle\Validator\Rule\Alnum;
use Particle\Validator\Rule\Alpha;
use Particle\Validator\Rule\Between;
use Particle\Validator\Rule\Boolean;
use Particle\Validator\Rule\Callback;
use Particle\Validator\Rule\CreditCard;
use Particle\Validator\Rule\Datetime;
use Particle\Validator\Rule\Digits;
use Particle\Validator\Rule\Each;
use Particle\Validator\Rule\Email;
use Particle\Validator\Rule\Equal;
use Particle\Validator\Rule\GreaterThan;
use Particle\Validator\Rule\InArray;
use Particle\Validator\Rule\Integer;
use Particle\Validator\Rule\IsArray;
use Particle\Validator\Rule\IsFloat;
use Particle\Validator\Rule\IsString;
use Particle\Validator\Rule\Json;
use Particle\Validator\Rule\Length;
use Particle\Validator\Rule\LengthBetween;
use Particle\Validator\Rule\LessThan;
use Particle\Validator\Rule\NotEmpty;
use Particle\Validator\Rule\Numeric;
use Particle\Validator\Rule\Phone;
use Particle\Validator\Rule\Regex;
use Particle\Validator\Rule\Required;
use Particle\Validator\Rule\Url;
use Particle\Validator\Rule\Uuid;
use PDO;
use PDOException;

class Config
{
    public static function getConnection($host = "localhost", $db_name = "cvglobal_site", $username = "root", $password = "")
    {
        $conn = null;

        try {
            $conn = new PDO("mysql:host=" . $host . ";dbname=" . $db_name, $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $conn;
    }

    public static function error_message_data_master()
    {
        $data = [
            Alnum::NOT_ALNUM              => '{{ name }} hanya boleh di isi angka dan huruf.',
            Alpha::NOT_ALPHA              => '{{ name }} hanya boleh di isi  huruf.',
            Between::TOO_BIG              => '{{ name }} harus kurang atau sama dengan {{ max }}',
            Between::TOO_SMALL            => '{{ name }} harus lebih atau sama dengan {{ min }}',
            Boolean::NOT_BOOL             => '{{ name }} harus bernilai true atau false',
            Boolean::NOT_BOOL             => '{{ name }} harus bernilai true atau false',
            Callback::INVALID_VALUE       => '{{ name }} tidak valid',
            CreditCard::INVALID_FORMAT    => '{{ name }} kartu kredit tidak valid',
            CreditCard::INVALID_CHECKSUM  => '{{ name }} kartu kredit tidak valid',
            Datetime::INVALID_VALUE       => '{{ name }} bukan tanggal valid',
            Digits::NOT_DIGITS            => '{{ name }} hanya boleh di isi angka ',
            Each::NOT_AN_ARRAY            => '{{ name }} harus sebuah array',
            Email::INVALID_FORMAT         => '{{ name }} format email salah',
            Equal::NOT_EQUAL              => '{{ name }} nilai harus sama dengan "{{ testvalue }}"',
            GreaterThan::NOT_GREATER_THAN => '{{ name }} harus lebih dari {{ min }}',
            InArray::NOT_IN_ARRAY         => '{{ name }} harus ada di dalam daftar',
            Integer::NOT_AN_INTEGER       => '{{ name }} hanya boleh di isi angka',
            IsArray::NOT_AN_ARRAY         => '{{ name }} harus berbentuk array',
            IsFloat::NOT_A_FLOAT          => '{{ name }} harus berbentuk bilangan desimal',
            IsString::NOT_A_STRING        => '{{ name }} tidak valid',
            Json::INVALID_FORMAT          => '{{ name }} bukan format json yang valid',
            Length::TOO_SHORT             => '{{ name }} terlalu pendak  and jumlah karakter harus sama dengan {{ length }}',
            Length::TOO_LONG              => '{{ name }} terlalu panjang  and jumlah karakter harus sama dengan {{ length }}',
            LengthBetween::TOO_LONG       => '{{ name }} panjang karakter harus {{ max }} atau kurang',
            LengthBetween::TOO_SHORT      => '{{ name }} panjang karakter harus {{ min }} atau lebih',
            LessThan::NOT_LESS_THAN       => '{{ name }} harus kurang dari {{ max }}',
            NotEmpty::EMPTY_VALUE         => '{{ name }} harus di isi',
            Numeric::NOT_NUMERIC          => '{{ name }} harus angka',
            Phone::INVALID_VALUE          => '{{ name }} nomor telp tidak valid',
            Phone::INVALID_FORMAT         => '{{ name }} nomor telp tidak valid',
            Regex::NO_MATCH               => '{{ name }} tidak valid',
            Required::NON_EXISTENT_KEY    => '{{ key }} harus di isi',
            Url::INVALID_URL              => '{{ name }} tidak valid',
            Url::INVALID_SCHEME           => '{{ name }} must have one of the following schemes: {{ schemes }}',
            Uuid::INVALID_UUID            => '{{ name }} tidak valid (v{{ version }})',
        ];

        return $data;
    }


}