<?php

error_reporting(E_ALL && ~E_NOTICE);
require "vendor/autoload.php";

$app = new \Slim\Slim();
define('URLS', $app->request->getRootUri());
$pendaftar = new \Controller\PendaftarController();
$page = new \Controller\PageController();
$tool = new \Controller\ToolController();

$app->get('/admin', function () use ($page, $app) {
    $page->admin();
});
$app->get('/programmer', function () use ($page, $app) {
    $page->programmer();
});
$app->get('/pendaftar', function () use ($pendaftar, $app) {
    $pendaftar->index();
});
$app->get('/pendaftar/:type/:stat/list', function ($type, $stat) use ($pendaftar, $app) {
    $pendaftar->indexbytype($type, $stat);
});
$app->get('/pendaftar/:type/:stat/create', function ($type, $stat) use ($pendaftar, $app) {
    $pendaftar->create($type, $stat);
});
$app->get('/pendaftar/:type/:stat/update/:id', function ($type, $stat, $id) use ($pendaftar, $app) {
    $pendaftar->edit($id, $type, $stat);
});
$app->get('/pendaftar/:type/:stat/delete/:id', function ($type, $stat, $id) use ($pendaftar, $app) {
    $pendaftar->delete($id, $type, $stat);
});
$app->post('/pendaftar/:type/:stat/create', function ($type, $stat) use ($pendaftar, $app) {
    $pendaftar->insert($type, $stat, $app->request()->post(), $_FILES['img'], $_FILES['jls']);
});
$app->post('/pendaftar/:type/:stat/update/:id', function ($type, $stat, $id) use ($pendaftar, $app) {
    $pendaftar->update($id, $type, $stat, $app->request()->post(), $_FILES['img'], $_FILES['jls']);
});
$app->get('/tool', function () use ($tool, $app) {
    $data = $tool->all();

});
function buatrp($angka)
{
    $jadi = "Rp " . number_format($angka, 2, ',', '.');

    return $jadi;
}

function echoResponse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
    // setting response content type to json
    $app->contentType('application/json');
    echo json_encode($response);
}

$app->run();