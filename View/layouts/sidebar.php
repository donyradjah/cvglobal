<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <div class="user-details">
                <div class="overlay"></div>
                <div class="text-center">
                    <img style="background: white" src="<?php echo URLS ?>/assets/images/logo3.png" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div>
<!--                        <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown"-->
<!--                           aria-expanded="false">--><?php //echo $_SESSION['nama'] . " - " . $_SESSION['email'] ?><!-- <span-->
<!--                                    class="mdi mdi-menu-down"></span></a>-->
                    </div>
                </div>
            </div>

            <div class="dropdown" id="setting-dropdown">
                <ul class="dropdown-menu">
                    <li><a href="<?php echo URLS ?>/logout"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>
                </ul>
            </div>

            <ul>
                <li class="menu-title">Navigation</li>

                <li>
                    <a href="<?php echo URLS ?>/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span>
                    </a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi mdi-account-circle"></i>
                        <span> Data Pendaftar Riset </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo URLS ?>/pendaftar/1/new/list">Baru</a></li>
                        <li><a href="<?php echo URLS ?>/pendaftar/1/decline/list">Tolak</a></li>
                        <li><a href="<?php echo URLS ?>/pendaftar/1/accept/list">Terima</a></li>
                    </ul>
                </li>

<!--                --><?php
//                if ($_SESSION['level'] > 100) { ?>
<!--                    <li>-->
<!--                        <a href="--><?php //echo URLS ?><!--/user" class="waves-effect"><i class="mdi mdi mdi-account-box"></i>-->
<!--                            <span> Data User </span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                --><?php //} ?>

            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>


    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->