<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Dashboard </h4>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="card-box widget-box-two widget-two-info">
                        <i class="mdi mdi-book  widget-two-icon"></i>
                        <div class="wigdet-two-content text-white">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">
                                Pendaftar Riset baru</p>
                            <h2 class="text-white"><span data-plugin="counterup">34578</span>
                            </h2>
                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-4 col-md-6">
                    <div class="card-box widget-box-two widget-two-primary">
                        <i class="mdi mdi-layers widget-two-icon"></i>
                        <div class="wigdet-two-content text-white">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">
                                Pendaftar Proyek baru</p>
                            <h2 class="text-white"><span data-plugin="counterup">52410 </span></h2>

                        </div>
                    </div>
                </div><!-- end col -->

                <div class="col-lg-4 col-md-6">
                    <div class="card-box widget-box-two widget-two-danger">
                        <i class="mdi mdi-amplifier widget-two-icon"></i>
                        <div class="wigdet-two-content text-white">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">
                                Pendaftar Alat baru</p>
                            <h2 class="text-white"><span data-plugin="counterup">6352</span>
                            </h2>

                        </div>
                    </div>
                </div><!-- end col -->


            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-heading">
                        <h3 class="panel-title">Progress Programmer</h3>
                    </div>
                    <div class="card-box table-responsive">
                        <table id="datatable-responsive" class="table table-hover display ">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th class="col-md-2">Programmer</th>
                                <th class="col-md-3">Project</th>
                                <th class="col-md-4">Progress terakhir</th>
                                <th class="col-md-2">Tanggal</th>
                                <th class="col-md-1">persentase</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->


</div>


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<!-- Right Sidebar -->
<div class="side-bar right-bar">
    <a href="javascript:void(0);" class="right-bar-toggle">
        <i class="mdi mdi-close-circle-outline"></i>
    </a>
    <h4 class="">Settings</h4>
    <div class="setting-list nicescroll">
        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">Notifications</h5>
                <p class="text-muted m-b-0">
                    <small>Do you need them?</small>
                </p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">API Access</h5>
                <p class="m-b-0 text-muted">
                    <small>Enable/Disable access</small>
                </p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">Auto Updates</h5>
                <p class="m-b-0 text-muted">
                    <small>Keep up to date</small>
                </p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-8">
                <h5 class="m-0">Online Status</h5>
                <p class="m-b-0 text-muted">
                    <small>Show your status to all</small>
                </p>
            </div>
            <div class="col-xs-4 text-right">
                <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
            </div>
        </div>
    </div>
</div>
<!-- /Right-bar -->

</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo URLS ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URLS ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo URLS ?>/assets/js/detect.js"></script>
<script src="<?php echo URLS ?>/assets/js/fastclick.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.blockUI.js"></script>
<script src="<?php echo URLS ?>/assets/js/waves.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.js"></script>

<!-- Counter js  -->
<script src="<?php echo URLS ?>/assets/plugins/waypoints/jquery.waypoints.min.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- Flot chart js -->
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.min.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.time.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.pie.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.selection.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>

<script src="<?php echo URLS ?>/assets/plugins/moment/moment.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>


<!-- Dashboard init -->
<script src="<?php echo URLS ?>/assets/pages/jquery.dashboard_2.js"></script>

<!-- App js -->
<script src="<?php echo URLS ?>/assets/js/jquery.core.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.app.js"></script>

<script>
    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#reportrange').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2016',
        dateLimit: {
            days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-success',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Cancel',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    }, function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });
</script>

</body>
</html>