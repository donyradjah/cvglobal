<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <br>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <center><img src="<?php echo URLS; ?>/assets/images/darunnajah.png" width="40%"><br>
                            <h1 style="text-align: center"> PPAI DARUN NAJAH <br> KARANG PLOSO - MALANG</h1></center>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->


    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo URLS ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/detect.js"></script>
    <script src="<?php echo URLS ?>/assets/js/fastclick.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo URLS ?>/assets/js/waves.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.js"></script>

    <script src="<?php echo URLS ?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.scroller.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.colVis.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>

    <!-- init -->
    <script src="<?php echo URLS ?>/assets/pages/jquery.datatables.init.js"></script>

    <!-- App js -->
    <script src="<?php echo URLS ?>/assets/js/jquery.core.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#datatable-responsive').DataTable();

        });
        TableManageButtons.init();

    </script>
