<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Data Pendaftar </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li><a href="<?php echo URLS ?>/">Dashboard</a></li>
                            <li class="active">
                                <a href="<?php echo URLS ?>/"><?php echo($type == 1 ? "PENDAFTAR RISET" : "PENDAFTAR ALAT") ?></a>
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">


                        <h4 class="header-title m-t-0">Form
                            Data <?php echo($type == 1 ? "PENDAFTAR RISET" : "PENDAFTAR ALAT") ?></h4>
                        <?php
                        if ($success == false && isset($success)) {
                            ?>
                            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">

                                <i class="mdi mdi-block-helper"></i>
                                <strong>Terjadi Kesalahan - </strong> <?php echo $message ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="p-20">
                            <form method="post" data-parsley-validate novalidate enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="kode">Nama<span
                                                class="text-danger">*</span></label>
                                    <input type="text" name="nama" parsley-trigger="change" required
                                           placeholder="Nama" class="form-control" id="nama"
                                           value="<?php echo $data['nama'] ?>">
                                </div>
                                <div class="form-group">
                                    <label for="nim">NIM</label>
                                    <input type="text" name="nim" data-parsley-type="number" parsley-trigger="change"
                                           placeholder="NIM" class="form-control" id="nim"
                                           value="<?php echo $data['nim'] ?>">
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat<span
                                                class="text-danger">*</span></label>
                                    <input type="text" name="alamat" parsley-trigger="change"
                                           required
                                           placeholder="Alamat" class="form-control" id="alamat"
                                           value="<?php echo $data['alamat'] ?>">
                                </div>
                                <div class="form-group">
                                    <label for="telepon">Telepon<span
                                                class="text-danger">*</span></label>
                                    <input type="text" name="telepon" data-parsley-type="number"
                                           parsley-trigger="change"
                                           required
                                           placeholder="NO. TELP" class="form-control" id="telepon"
                                           value="<?php echo $data['telepon'] ?>">
                                </div>
                                <div class="form-group">
                                    <label for="asal_kampus">Asal Kampus</label>
                                    <input type="text" name="asal_kampus" parsley-trigger="change"
                                           placeholder="Asal Kampus" class="form-control" id="asal_kampus"
                                           value="<?php echo $data['asal_kampus'] ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" data-parsley-type="email" name="email" parsley-trigger="change"
                                           required placeholder="email" class="form-control" id="email"
                                           value="<?php echo $data['email'] ?>">
                                </div>

                                <div class="form-group">
                                    <label for="paket">Paket:</label>
                                    <select parsley-trigger="change" name="paket" id="paket"
                                            class="form-control selectpicker"
                                            data-live-search="true" required>
                                        <option value="">-- Pilih paket --</option>
                                        <?php foreach ($paket['data'] as $row) {
                                            ?>
                                            <option <?php if ($data['paket'] == $row['id']) {
                                                echo "selected";
                                            } ?> value="<?php echo $row['id'] ?>"><?php echo $row['nama_paket'] ?></option>
                                            <?php
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="hargatxt">Harga</label>
                                    <input type="text" name="hargatxt" parsley-trigger="change"
                                           required placeholder="Harga Paket" class="form-control" id="hargatxt"
                                           value="<?php echo $data['harga'] ?>" onchange="FormatCurrency(this,'harga')"
                                           onfocus="FormatCurrency(this,'harga')" onload="FormatCurrency(this,'harga')"
                                           onkeyup="FormatCurrency(this,'harga')">
                                    <input type="hidden" name="harga" id="harga" value="<?php echo $data['harga'] ?>">
                                    <input type="hidden" name="agreements" id="agreements" value="1">
                                    <input type="hidden" name="status" id="status" value="accept">
                                    <input type="hidden" name="type" id="type" value="<?php echo $type ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label"> Bukti pembayaran DP</label>
                                    <input type="file" class="filestyle" name="img" data-input="false">
                                </div>
                                <div class="form-group">
                                    <label class="control-label"> Nominal DP</label>
                                    <input type="text" name="nominaltxt" parsley-trigger="change"
                                           required placeholder="Nominal DP" class="form-control" id="hargatxt"
                                           value="<?php echo $data['nominal'] ?>"
                                           onchange="FormatCurrency(this,'nominal')"
                                           onfocus="FormatCurrency(this,'nominal')"
                                           onload="FormatCurrency(this,'nominal')"
                                           onkeyup="FormatCurrency(this,'nominal')">
                                    <input type="hidden" name="nominal" id="nominal"
                                           value="<?php echo $data['nominal'] ?>">
                                </div>
                                <div class="form-group">
                                    <label class="control-label"> Judul</label>
                                    <textarea name="judul" class="form-control"
                                              parsley-trigger="change"><?php echo $data['judul'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"> Penjelasan Program</label>
                                    <input type="file" class="filestyle" name="jls" data-input="false">
                                </div>
                                <div class="form-group">
                                    <label for="paket">Tool:</label>
                                    <select parsley-trigger="change" name="tool" id="tool"
                                            class="form-control selectpicker"
                                            data-live-search="true">
                                        <option value="">-- Pilih tool --</option>
                                        <?php foreach ($tool['data'] as $row) {
                                            ?>
                                            <option <?php if ($data['tool'] == $row['tool']) {
                                                echo "selected";
                                            } ?> value="<?php echo $row['tool'] ?>"><?php echo $row['tool'] ?></option>
                                            <?php
                                        } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"> Catatan</label>
                                    <textarea name="catatan" class="form-control"
                                              parsley-trigger="change"><?php echo $data['catatan'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="btn-switch btn-switch-custom">
                                        <input type="checkbox" name="kirim_email" value="1"
                                               id="input-btn-switch-custom"/>
                                        <label for="input-btn-switch-custom"
                                               class="btn btn-rounded btn-custom waves-effect waves-light">
                                            <em class="glyphicon glyphicon-ok"></em>
                                            <strong> Kirim Email</strong>
                                        </label>
                                    </div>
                                </div>
                        </div>
                        <div class="form-group text-right m-b-0">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                Simpan
                            </button>
                            <a href="<?php echo URLS ?>/pendaftar/<?php echo $type ?>/list/accept"
                               class="btn btn-danger waves-effect m-l-5">
                                Batal
                            </a>
                        </div>

                        </form>

                    </div>


                </div>
                <!-- end row -->


            </div><!-- end col-->

        </div>
        <!-- end row -->


    </div> <!-- container -->

</div> <!-- content -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo URLS ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URLS ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo URLS ?>/assets/js/detect.js"></script>
<script src="<?php echo URLS ?>/assets/js/fastclick.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.blockUI.js"></script>
<script src="<?php echo URLS ?>/assets/js/waves.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.js"></script>

<script src="<?php echo URLS ?>/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript"
        src="<?php echo URLS ?>/assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript"
        src="<?php echo URLS ?>/assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-select/js/bootstrap-select.min.js"
        type="text/javascript"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js"
        type="text/javascript"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"
        type="text/javascript"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
        type="text/javascript"></script>
<script src="<?php echo URLS ?>/assets/plugins/moment/moment.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo URLS ?>/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
        type="text/javascript"></script>
<script src="<?php echo URLS ?>/assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo URLS ?>/assets/plugins/parsleyjs/parsley.min.js"></script>

<!-- App js -->
<script src="<?php echo URLS ?>/assets/js/jquery.core.js"></script>
<script src="<?php echo URLS ?>/assets/js/jquery.app.js"></script>

<script type="text/javascript">
    function FormatCurrency(objNum, id) {

        var num = objNum.value;
        $('#' + id).val(num.replace(/[^0-9]/g, ''));
        var ent, dec;
        if (num != '' && num != objNum.oldvalue) {
            num = HapusTitik(num);
            if (isNaN(num)) {
                objNum.value = (objNum.oldvalue) ? objNum.oldvalue : '';
            } else {
                var ev = (navigator.appName.indexOf('Netscape') != -1) ? Event : event;
                if (ev.keyCode == 190 || !isNaN(num.split('.')[1])) {
                    alert(num.split('.')[1]);
                    objNum.value = TambahTitik(num.split('.')[0]) + '.' + num.split('.')[1];
                } else {
                    objNum.value = TambahTitik(num.split('.')[0]);
                }
                objNum.oldvalue = objNum.value;
            }
        }
    }
    function HapusTitik(num) {
        return (num.replace(/\./g, ''));
    }
    function TambahTitik(num) {
        numArr = new String(num).split('').reverse();
        for (i = 3; i < numArr.length; i += 3) {
            numArr[i] += '.';
        }
        return numArr.reverse().join('');
    }
    Parsley.addMessages('id', {
        defaultMessage: "tidak valid",
        type: {
            email: "email tidak valid",
            url: "url tidak valid",
            number: "harus angka",
            integer: "harus angka",
            digits: "harus berupa digit",
            alphanum: "harus berupa alphanumeric"
        },
        notblank: "tidak boleh kosong",
        required: "tidak boleh kosong",
        pattern: "tidak valid",
        min: "harus lebih besar atau sama dengan %s.",
        max: "harus lebih kecil atau sama dengan %s.",
        range: "harus dalam rentang %s dan %s.",
        minlength: "terlalu pendek, minimal %s karakter atau lebih.",
        maxlength: "terlalu panjang, maksimal %s karakter atau kurang.",
        length: "panjang karakter harus dalam rentang %s dan %s",
        mincheck: "pilih minimal %s pilihan",
        maxcheck: "pilih maksimal %s pilihan",
        check: "pilih antar %s dan %s pilihan",
        equalto: "harus sama"
    });

    Parsley.setLocale('id');
    $(document).ready(function () {
        $('form').parsley();
        $('.selectpicker').selectpicker();
    });

</script>
