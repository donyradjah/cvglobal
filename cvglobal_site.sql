/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : cvglobal_site

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-01-31 15:47:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bahasa_pelamar
-- ----------------------------
DROP TABLE IF EXISTS `bahasa_pelamar`;
CREATE TABLE `bahasa_pelamar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bahasa` varchar(50) DEFAULT NULL,
  `mendengar` varchar(50) DEFAULT NULL,
  `berbicara` varchar(50) DEFAULT NULL,
  `membaca` varchar(50) DEFAULT NULL,
  `menulis` varchar(50) DEFAULT NULL,
  `id_pelamar` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bahasa_pelamar
-- ----------------------------

-- ----------------------------
-- Table structure for bahasa_program
-- ----------------------------
DROP TABLE IF EXISTS `bahasa_program`;
CREATE TABLE `bahasa_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bahasa_pemrograman` varchar(50) DEFAULT NULL,
  `coding_umum` varchar(50) DEFAULT NULL,
  `coding_aritmatika` varchar(50) DEFAULT NULL,
  `id_pelamar` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bahasa_program
-- ----------------------------

-- ----------------------------
-- Table structure for diskon
-- ----------------------------
DROP TABLE IF EXISTS `diskon`;
CREATE TABLE `diskon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` longtext,
  `telepon` varchar(25) DEFAULT NULL,
  `omset` int(11) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `platform` int(11) DEFAULT NULL,
  `renewer` int(11) DEFAULT NULL,
  `harga_perpanjangan` double DEFAULT NULL,
  `alur` longtext,
  `nama_website` varchar(255) DEFAULT NULL,
  `rujukan_website` varchar(255) DEFAULT NULL,
  `garansi` int(11) DEFAULT NULL,
  `marketing` int(11) DEFAULT NULL,
  `harga_user` double DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `fitur_user` longtext,
  `fitur` longtext,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of diskon
-- ----------------------------

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of image
-- ----------------------------

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `php_sapi_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `context` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of logs
-- ----------------------------

-- ----------------------------
-- Table structure for marketing
-- ----------------------------
DROP TABLE IF EXISTS `marketing`;
CREATE TABLE `marketing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_marketing` varchar(35) DEFAULT NULL,
  `alamat` longtext,
  `telepon` varchar(15) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `nik_marketing` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of marketing
-- ----------------------------

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `messages` longtext,
  `fromid` int(11) DEFAULT NULL,
  `sentat` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `toid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for paket
-- ----------------------------
DROP TABLE IF EXISTS `paket`;
CREATE TABLE `paket` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_paket` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `kode` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of paket
-- ----------------------------
INSERT INTO `paket` VALUES ('1', 'Paket A (Judul dibimbing dari Kantor)', 'Judul + Proposal + laporan skripsi lengkap', '4500000', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null, 'RST/A');
INSERT INTO `paket` VALUES ('2', 'Paket A (Judul dari Pendaftar)', 'Judul + Proposal + laporan skripsi lengkap', '0', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null, 'RST/A');
INSERT INTO `paket` VALUES ('3', 'Paket B', 'Program (Software)', '0', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null, 'RST/B');
INSERT INTO `paket` VALUES ('4', 'Paket C (Judul dibimbing dari Kantor)', 'Judul + Proposal + laporan skripsi lengkap + Program (Software)', '7000000', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null, 'RST/C');
INSERT INTO `paket` VALUES ('5', 'Paket C (Judul dari Pendaftar)', 'Judul + Proposal + laporan skripsi lengkap + Program (Software)', '0', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null, 'RST/C');
INSERT INTO `paket` VALUES ('6', 'Paket Alat', 'Alat', '0', '2016-10-03 13:40:59', '0000-00-00 00:00:00', null, 'ALT');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for pelamar
-- ----------------------------
DROP TABLE IF EXISTS `pelamar`;
CREATE TABLE `pelamar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan` varchar(20) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `tempat_lhr` varchar(50) DEFAULT NULL,
  `tanggal_lhr` date DEFAULT NULL,
  `jk` varchar(5) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `agama` varchar(15) DEFAULT NULL,
  `kewarganegaraan` varchar(35) DEFAULT NULL,
  `no_ktp` varchar(30) DEFAULT NULL,
  `no_sim` varchar(30) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status_tempat_tinggal` varchar(20) DEFAULT NULL,
  `hobby` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `lamaran` longtext,
  `foto` longtext,
  `ktp` longtext,
  `ijazah` longtext,
  `sertifikat` longtext,
  `pengalaman` longtext,
  `alasan` varchar(255) DEFAULT NULL,
  `gaji_terakhir` double(255,0) DEFAULT NULL,
  `fasilitas` longtext,
  `zip` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `created_at` (`created_at`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `telepon` (`telepon`),
  UNIQUE KEY `no_ktp` (`no_ktp`),
  KEY `created_at_2` (`created_at`),
  KEY `created_at_3` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelamar
-- ----------------------------

-- ----------------------------
-- Table structure for pelamar_program
-- ----------------------------
DROP TABLE IF EXISTS `pelamar_program`;
CREATE TABLE `pelamar_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(75) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `waktu` varchar(50) DEFAULT NULL,
  `id_pelamar` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pelamar_program
-- ----------------------------

-- ----------------------------
-- Table structure for pemasukan
-- ----------------------------
DROP TABLE IF EXISTS `pemasukan`;
CREATE TABLE `pemasukan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asal` varchar(50) DEFAULT NULL,
  `nominal` double DEFAULT NULL,
  `id_pendaftar` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pemasukan
-- ----------------------------

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `jumlah_setor` double NOT NULL,
  `penyetor` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `img` longtext COLLATE utf8_unicode_ci,
  `tipe` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sisa` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------

-- ----------------------------
-- Table structure for pendaftar
-- ----------------------------
DROP TABLE IF EXISTS `pendaftar`;
CREATE TABLE `pendaftar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asal_kampus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `judul` longtext COLLATE utf8_unicode_ci NOT NULL,
  `paket` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `agreements` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nominal` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `penjelasan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tool` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `catatan` longtext COLLATE utf8_unicode_ci,
  `tanggal` date DEFAULT NULL,
  `no_urut` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of pendaftar
-- ----------------------------
INSERT INTO `pendaftar` VALUES ('1', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '111', '0000-00-00', '111');
INSERT INTO `pendaftar` VALUES ('2', 'Fitri Yani', '1112094000021', 'Ibnu Khaldun no 3 Ciputat Tangerang Selatan', '083872901029', 'UIN Syarif Hidayatullah Jakarta', 'yani60232@gmail.com', 'Judul riset', '4', '7000000', '1', 'accept', 'set/public/images/051020160336fitriyani.jpg', '500000', '1', 'set/public/dokumen/051020160336fitriyani', 'UNITY', '2016-10-05 10:36:41', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('3', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '1111', '0000-00-00', '1111');
INSERT INTO `pendaftar` VALUES ('4', 'Norma Ayu W', '-', 'Solo, Kadipiro', '082226544675', '-', 'nurmawiga@gmail.com', 'Sistem Aplikasi Ujian Online', '3', '4000000', '1', 'accept', 'set/public/images/151020160442normaayuw.jpg', '4000000', '1', 'set/public/dokumen/151020160442normaayuwujian.zip', 'CODEIGNITER', '2016-10-15 23:42:10', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('5', 'Habbadal Murtafa', '15112281', 'JL. Pesantren No. 51', '085257231133', 'STIKMA', 'dalz.resevers@gmail.com', 'Sistem Administrasi Pesantren Salafiyah PPAI Nurun Najah', '5', '8000000', '1', 'accept', '', '0', '1', '', 'Laravel', '2016-10-22 11:11:03', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('6', 'bella shofia', '-', 'dago asri 20,bandung', '081230246367', '-', 'bellasof95@gmail.com', 'Judul riset : ASR Bahasa Indonesia', '3', '8000000', '1', 'accept', 'set/public/images/311020160416bellashofia.jpg', '1000000', '1', 'set/public/dokumen/311020160416bellashofiaASR.docx', 'Java+Eksperimen dengan tools SRILM dan KALDI', '2016-10-31 16:16:24', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('7', 'Tri Mulya Abadi', '201110370311061', 'Jetis Malang', '085785229904', 'UMM', 'treeckzmolarz@gmail.com', 'Pengenalan Pola Batik', '5', '5500000', '1', 'accept', '', '500000', '1', '', 'Java', '2016-05-11 08:37:48', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('8', 'Dony Rinaldo Radjah', '123456', 'donyradjah@cv-globalsolusindo.com', '08813146431', 'ITN', 'donyradjah@cv-globalsolusindo.com', 'Aplikasi Chatting dengan google cloud messaging', '0', '4500000', '1', 'new', '', '1000000', '0', '', 'MATLAB', '2016-11-02 10:26:56', '2017-01-30 16:03:18', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('9', 'Dony Rinaldo Radjah', '123456', 'Malang', '08813146431', 'ITN', 'admin@cv-globalsolusindo.com', 'Aplikasi Chatting dengan google cloud messaging', '1', '4500000', '1', 'accept', '', '2000000', '1', '', 'Java', '2016-11-02 10:28:51', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('10', 'Dony Rinaldo Radjah', '123456', 'donyradjah@cv-globalsolusindo.com', '08813146431', 'ITN', 'donyradjah@cv-globalsolusindo.com', 'Aplikasi Chatting dengan google cloud messaging', '1', '4500000', '1', 'accept', '', '1000000', '1', '', 'MATLAB', '2016-11-01 10:30:24', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('11', 'Dony Rinaldo Radjah', '123456', 'admin@cv-globalsolusindo.com', '08813146431', 'ITN', 'donyradjah@gmail.com', 'Sistem Administrasi Pesantren Salafiyah PPAI Nurun Najah 55', '0', '4500000', '1', 'accept', '', '1000000', '1', '', 'MATLAB', '2016-11-02 10:31:19', '2017-01-30 16:05:36', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('23', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '111', '0000-00-00', '111');
INSERT INTO `pendaftar` VALUES ('24', 'Fitri Yani', '1112094000021', 'Ibnu Khaldun no 3 Ciputat Tangerang Selatan', '083872901029', 'UIN Syarif Hidayatullah Jakarta', 'yani60232@gmail.com', 'Judul riset', '4', '7000000', '1', 'accept', 'set/public/images/051020160336fitriyani.jpg', '500000', '1', 'set/public/dokumen/051020160336fitriyani', 'UNITY', '2016-10-05 10:36:41', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('25', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '1111', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null, '1111', '0000-00-00', '1111');
INSERT INTO `pendaftar` VALUES ('26', 'Norma Ayu W', '-', 'Solo, Kadipiro', '082226544675', '-', 'nurmawiga@gmail.com', 'Sistem Aplikasi Ujian Online', '3', '4000000', '1', 'accept', 'set/public/images/151020160442normaayuw.jpg', '4000000', '1', 'set/public/dokumen/151020160442normaayuwujian.zip', 'CODEIGNITER', '2016-10-15 23:42:10', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('27', 'Habbadal Murtafa', '15112281', 'JL. Pesantren No. 51', '085257231133', 'STIKMA', 'dalz.resevers@gmail.com', 'Sistem Administrasi Pesantren Salafiyah PPAI Nurun Najah', '5', '8000000', '1', 'accept', '', '0', '1', '', 'Laravel', '2016-10-22 11:11:03', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('28', 'bella shofia', '-', 'dago asri 20,bandung', '081230246367', '-', 'bellasof95@gmail.com', 'Judul riset : ASR Bahasa Indonesia', '3', '8000000', '1', 'accept', 'set/public/images/311020160416bellashofia.jpg', '1000000', '1', 'set/public/dokumen/311020160416bellashofiaASR.docx', 'Java+Eksperimen dengan tools SRILM dan KALDI', '2016-10-31 16:16:24', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('29', 'Tri Mulya Abadi', '201110370311061', 'Jetis Malang', '085785229904', 'UMM', 'treeckzmolarz@gmail.com', 'Pengenalan Pola Batik', '5', '5500000', '1', 'accept', '', '500000', '1', '', 'Java', '2016-05-11 08:37:48', '0000-00-00 00:00:00', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('30', 'Dony Rinaldo Radjah', '123456', 'donyradjah@cv-globalsolusindo.com', '08813146431', 'ITN', 'donyradjah@cv-globalsolusindo.com', 'Aplikasi Chatting dengan google cloud messaging', '0', '4500000', '1', 'new', '', '1000000', '0', '', 'MATLAB', '2016-11-02 10:26:56', '2017-01-30 16:03:18', null, null, null, null);
INSERT INTO `pendaftar` VALUES ('31', 'Dony Rinaldo Radjah', '123456', 'Malang', '08813146431', 'ITN', 'admin@cv-globalsolusindo.com', 'Aplikasi Chatting dengan google cloud messaging', '1', '4500000', '1', 'accept', '', '2000000', '1', '', 'Java', '2016-11-02 10:28:51', '0000-00-00 00:00:00', null, null, null, null);

-- ----------------------------
-- Table structure for pendaftar_proyek
-- ----------------------------
DROP TABLE IF EXISTS `pendaftar_proyek`;
CREATE TABLE `pendaftar_proyek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(75) DEFAULT NULL,
  `bidang` varchar(50) DEFAULT NULL,
  `proposal` longtext,
  `jangka_waktu` date DEFAULT NULL,
  `training` varchar(15) DEFAULT NULL,
  `garansi` double DEFAULT NULL,
  `penerapan` varchar(20) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `img` longtext,
  `nominal` double DEFAULT NULL,
  `telepon` varchar(12) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `penjelasan` longtext,
  `produk` int(11) DEFAULT NULL,
  `status` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pendaftar_proyek
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc1` longtext COLLATE utf8_unicode_ci NOT NULL,
  `desc2` longtext COLLATE utf8_unicode_ci NOT NULL,
  `desc3` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of product
-- ----------------------------

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_produk` varchar(75) DEFAULT NULL,
  `tool` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `harga` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of produk
-- ----------------------------

-- ----------------------------
-- Table structure for progress
-- ----------------------------
DROP TABLE IF EXISTS `progress`;
CREATE TABLE `progress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `programmer` int(11) DEFAULT NULL,
  `pendaftar` int(11) DEFAULT NULL,
  `progress` double DEFAULT NULL,
  `isi_progress` longtext,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `tool` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of progress
-- ----------------------------

-- ----------------------------
-- Table structure for progress_ambil
-- ----------------------------
DROP TABLE IF EXISTS `progress_ambil`;
CREATE TABLE `progress_ambil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `pendaftar` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of progress_ambil
-- ----------------------------

-- ----------------------------
-- Table structure for riwayat_pekerjaan
-- ----------------------------
DROP TABLE IF EXISTS `riwayat_pekerjaan`;
CREATE TABLE `riwayat_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perusahaan` varchar(50) DEFAULT NULL,
  `jenis_usaha` varchar(50) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `id_pelamar` int(255) DEFAULT NULL,
  `dari` varchar(20) NOT NULL,
  `sampai` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of riwayat_pekerjaan
-- ----------------------------

-- ----------------------------
-- Table structure for riwayat_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `riwayat_pendidikan`;
CREATE TABLE `riwayat_pendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(50) DEFAULT NULL,
  `nama_sekolah` varchar(50) DEFAULT NULL,
  `jurusan` varchar(50) DEFAULT NULL,
  `dari` varchar(50) DEFAULT NULL,
  `sampai` varchar(50) DEFAULT NULL,
  `id_pelamar` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of riwayat_pendidikan
-- ----------------------------

-- ----------------------------
-- Table structure for seminar
-- ----------------------------
DROP TABLE IF EXISTS `seminar`;
CREATE TABLE `seminar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tema` varchar(50) DEFAULT NULL,
  `penyelenggara` varchar(50) DEFAULT NULL,
  `waktu` date DEFAULT NULL,
  `id_pelamar` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of seminar
-- ----------------------------

-- ----------------------------
-- Table structure for tool
-- ----------------------------
DROP TABLE IF EXISTS `tool`;
CREATE TABLE `tool` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tool` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tool
-- ----------------------------
INSERT INTO `tool` VALUES ('1', 'Java', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('2', 'VB.NET', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('3', 'MATLAB', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('4', 'IONIC', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('5', 'REACT JS', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('6', 'ActionScript', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('7', 'ANGULAR JS', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('8', 'PHP', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('9', 'NODE JS', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('10', 'WORDPRESS', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('11', 'FEATHERS', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('12', 'CODEIGNITER', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('13', 'Ruby On Rails', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('14', 'Laravel', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('15', 'ASP.NET', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('16', 'UNITY', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('17', 'Arduino', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('18', 'Rasberry pi', '2016-09-21 09:04:50', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('19', 'Laporan', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `tool` VALUES ('20', 'Python', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of type
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nomer_telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Admin CV.GLOBALSOLUSINDO', '085655580008', 'admin@cv-globalsolusindo.com', '$2y$10$MWCgVhhErUPIbW3YrH3VrObpzqn4mWwFbTU.V/6YdyO41JS3dGIJW', '100', 'QC1QTPePCibm19Ty6HmPPPpw6QihhiH3CiaGGxUy64NrwuCJFdI2zn3dqxzZ', '2016-09-21 09:04:50', '2016-11-16 14:42:51');
INSERT INTO `users` VALUES ('2', 'Dony Rinaldo Radjah', '08813146431', 'donyradjah@cv-globalsolusindo.com', '$2y$10$MWCgVhhErUPIbW3YrH3VrObpzqn4mWwFbTU.V/6YdyO41JS3dGIJW', '200', '7ZCvff4whmNiBvqJxkWEzAQrQokW83d2F2ALGDlYEl15QxMQpXMLT6OYQAi3', '2016-09-21 09:04:50', '2016-11-16 15:07:29');

-- ----------------------------
-- Table structure for user_pendaftar
-- ----------------------------
DROP TABLE IF EXISTS `user_pendaftar`;
CREATE TABLE `user_pendaftar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pendaftar` int(11) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_pendaftar
-- ----------------------------
